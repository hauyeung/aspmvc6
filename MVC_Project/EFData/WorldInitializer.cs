﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using MVC_Project.EFData;
using MVC_Project.Models;

namespace MVC_Project.EFData
{
    public class WorldInitializer : DropCreateDatabaseIfModelChanges<WorldContext>
    {
        protected override void Seed(WorldContext context)
        {
            var continents = new List<Continents>
            {
                new Continents{ ContinentName = "Asia", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>() },
                new Continents{ ContinentName = "Africa", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>() },
                new Continents{ ContinentName = "North America", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>()},
                new Continents{ ContinentName = "South America", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>()},
                new Continents{ ContinentName = "Antarctica", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>()},
                new Continents{ ContinentName = "Europe", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>()},
                new Continents{ ContinentName = "Australia", CountriesList = new List<Countries>(), travellerslist = new List<Travellers>()},
            };
            continents.ForEach(s => context.continents.Add(s));
            context.SaveChanges();

            var countries = new List<Countries>
            {
                new Countries{countryname = "Austria", currencyname = "EURO", conversionrate=0.77, fkcontinent=6, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Brazil", currencyname = "REAL", conversionrate=2.14, fkcontinent=4, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Canada", currencyname = "DOLLAR", conversionrate=1.04, fkcontinent=3, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Chile", currencyname = "PESO", conversionrate=504.40, fkcontinent=4, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Algeria", currencyname = "DINAR", conversionrate=79.07, fkcontinent=2, travellerslist = new List<Travellers>()},
                new Countries{countryname = "China", currencyname = "YUAN", conversionrate=6.131, fkcontinent=1, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Costa Rica", currencyname = "COLON", conversionrate=0.77, fkcontinent=4, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Poland", currencyname = "ZLOTY", conversionrate=3.30, fkcontinent=6, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Croatia", currencyname = "KUNA", conversionrate=5.81, fkcontinent=6, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Switzerland", currencyname = "FRANC", conversionrate=0.96, fkcontinent=6, travellerslist = new List<Travellers>()},
                new Countries{countryname = "Nicaragua", currencyname = "CORDOBA", conversionrate=24.12, fkcontinent=3, travellerslist = new List<Travellers>()},
            };
            countries.ForEach(s => context.countries.Add(s));
            context.SaveChanges();

            var travellers = new List<Travellers>
            {
                new Travellers{firstname="Sean", lastname="Long", countryvisited = "Croatia", rating=3.4, comments = "An amazing trip although I wish there was more grocery stores", fkcountriesid=9, fkcontinentalid=6},
                new Travellers{firstname="Kerry", lastname="Meir", countryvisited = "Poland", rating=4.7, comments = "Best ever!!!", fkcountriesid=8, fkcontinentalid=6},
                new Travellers{ firstname="Jerry", lastname="Lustenberg", countryvisited = "China", rating=3.1, comments = "Good but humid", fkcountriesid=8, fkcontinentalid=6},
                new Travellers{ firstname="Chandra", lastname="Lutwvig", countryvisited = "Brazil", rating=4.0, comments = "Absolutely BEAUTIFUL", fkcountriesid=2, fkcontinentalid=4},
                new Travellers{firstname="Mikayla", lastname="Chandrea", countryvisited = "Austria", rating=4.4, comments = "It was OK just really expensive", fkcountriesid=2, fkcontinentalid=4},
                new Travellers{firstname="Liam", lastname="Mikey", countryvisited = "Croatia", rating=1.7, comments = "Has a horrible time, no good tour guides.", fkcountriesid=9, fkcontinentalid=6},
                new Travellers{firstname="Michelle", lastname="Gombardo", countryvisited = "Nicaragua", rating=4.5, comments = "Food was delicious, but the bugs were horrendous., ", fkcountriesid=9, fkcontinentalid=6},
                new Travellers{firstname="Ferrari", lastname="Manchester", countryvisited = "Austria", rating=5.0, comments = "Best. Shopping. ", fkcountriesid=11, fkcontinentalid=3},
                new Travellers{firstname="Joshua", lastname="Blamberger", countryvisited = "Switzerland", rating=4.3, comments = "Beautiful country.", fkcountriesid=10, fkcontinentalid=6},
                new Travellers{firstname="Michelle", lastname="Gombardo", countryvisited = "Croatia", rating=3.4, comments = "An amnazing trip although I wish there was more grocery stores", fkcountriesid=9, fkcontinentalid=6},
                new Travellers{ firstname="Sean", lastname="Long", countryvisited = "Costa Rica", rating=3.9, comments = "Costa Rica", fkcountriesid=7, fkcontinentalid=3},
                new Travellers{firstname="Michelle", lastname="Gombardo", countryvisited = "Croatia", rating=3.4, comments = "An amnazing trip although I wish there was more grocery stores", fkcountriesid=9, fkcontinentalid=6},
                new Travellers{ firstname="Chandra", lastname="Lutvwig", countryvisited = "Canada", rating=2.4, comments = "Winter was far too harsh. Maybe a different season would be better?", fkcountriesid=3, fkcontinentalid=3},
                new Travellers{firstname="Jorge", lastname="Santoyo", countryvisited = "Algeria", rating=1.7, comments = " I didn't get to go on my hunting excursion because of the shoddy planning.", fkcountriesid=5, fkcontinentalid=2},
                new Travellers{firstname="Peter", lastname="Griffon", countryvisited = "Poland", rating=0.9, comments = " Poland", fkcountriesid=8, fkcontinentalid=6},
            };
            travellers.ForEach(s => context.travellers.Add(s));
            context.SaveChanges();
        }

    }
}
