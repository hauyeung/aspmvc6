﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Project.EFData;

namespace MVC_Project.Controllers
{
    public class ContinentsController : Controller
    {
        //
        // GET: /Continents/
        private WorldContext w = new WorldContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult continents()
        {
            return View(w.continents.ToList());
        }

    }
}
