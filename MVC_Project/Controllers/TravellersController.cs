﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Project.EFData;
using MVC_Project.Models;

namespace MVC_Project.Controllers
{
    public class TravellersController : Controller
    {
        //
        // GET: /Travellers/
        private WorldContext w = new WorldContext();
        public ActionResult Index()
        {
   
            return View(w.travellers.ToList());
        }
        public ActionResult addtravellers()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("addtravellers")]
        public ActionResult addtravellers(Travellers t)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            w.travellers.Add(t);
            w.SaveChanges();
            return RedirectToAction("Index","Home");
        }

    }
}
