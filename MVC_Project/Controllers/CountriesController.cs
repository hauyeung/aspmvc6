﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Project.EFData;
using MVC_Project.Models;

namespace MVC_Project.Controllers
{
    public class CountriesController : Controller
    {
        //
        // GET: /Countries/
        private WorldContext w = new WorldContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult countries()
        {
            return View(w.countries.ToList());
        }
        public ActionResult details(int id)
        {
            Countries country = w.countries.Find(id);
            return View(country);
        }
        public ActionResult countriesindex(int id)
        {
            Continents c = w.continents.Find(id);
            return View(c);
        }
        public ActionResult addcountry()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("addcountry")]
        public ActionResult addcountry(Countries c)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (w.countries.Where(d => d.countryname == c.countryname).SingleOrDefault() != null)
            {
                ViewBag.err = "Duplicate exists";
                return View();
            }
            w.countries.Add(c);
            w.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

    }
}
