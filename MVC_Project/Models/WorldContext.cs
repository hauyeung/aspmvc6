﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using MVC_Project.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MVC_Project.EFData
{
    public class WorldContext : DbContext
    {
        public DbSet<Continents> continents { get; set; }
        public DbSet<Countries> countries { get; set; }
        public DbSet<Travellers> travellers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();           
            modelBuilder.Entity<Countries>().HasOptional(a => a.continent).WithMany(b=>b.CountriesList).HasForeignKey(a=>a.fkcontinent);
            modelBuilder.Entity<Travellers>().HasRequired(c => c.continent).WithMany(d => d.travellerslist).HasForeignKey(c => c.fkcontinentalid);
            modelBuilder.Entity<Travellers>().HasRequired(e => e.country).WithMany(f => f.travellerslist).HasForeignKey(e=>e.fkcountriesid);
        }
    }

   
}
