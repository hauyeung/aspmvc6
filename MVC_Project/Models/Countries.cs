﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC_Project.Models
{
    public class Countries
    {
        [Key]
        public int ID { get; set; }
        public string countryname { get; set; }
        public string currencyname { get; set; }
        public double conversionrate { get; set; }
        public int? fkcontinent { get; set; }        
        public virtual ICollection<Travellers> travellerslist { get; set; }        
        public virtual Continents continent {get; set;}
    }
}
