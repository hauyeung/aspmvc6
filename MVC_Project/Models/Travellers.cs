﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC_Project.Models
{
    public class Travellers 
    {
        [Key]
        public int ID { get; set; }
        [StringLength(20, ErrorMessage="First name too long")]
        [Required]
        public string firstname { get; set; }        
        [StringLength(30, ErrorMessage="Last name too long")]
        public string lastname { get; set; }
        public string countryvisited { get; set; }
        [Range(0,5)]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:F2}")]
        public double rating { get; set; }
        [StringLength(300, ErrorMessage="Comment text too long")]
        public string comments { get; set; }
        public int fkcountriesid { get; set; }
        public int fkcontinentalid { get; set; }
        public virtual Continents continent { get; set; }
        public virtual Countries country { get; set; }
        
    }
}
