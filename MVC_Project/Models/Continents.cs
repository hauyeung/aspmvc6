﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace MVC_Project.Models
{
    public class Continents
    {
        [Key]
        public int  ContinentID { get; set; }
        public string ContinentName { get; set; }
        public virtual ICollection<Countries> CountriesList { get; set; }
        public virtual ICollection<Travellers> travellerslist {get; set;}
    }

     
}
